APP=insecure-programming-app

all: build

test:
	docker run --rm -i hadolint/hadolint:v1.17.1 < Dockerfile
	docker-compose config

securitytest:
	docker build -t $(APP)-test test/
	docker run --rm -i -v "C:\Users\MichaelSta\training\capability":/usr/src/app $(APP)-test

build:
	docker-compose build

run:
	docker-compose up

clean:
	docker-compose down
	docker-compose rm -f
	docker system prune

.PHONY: all test securitytest clean
